var path = require("path");

const MiniCssExtractPlugin = require("mini-css-extract-plugin");

module.exports = {

    entry: "./src/index.js",
    output: {
        path: path.join(__dirname, "dist", "assets"),
        filename: "bundle.js",
    },
    plugins: [new MiniCssExtractPlugin({filename: "style.css"})],
    module: {
        rules: [
            {
                test: /\.(jsx|js)$/,
                exclude: /(node_modules)/,
                use: {
                    loader: "babel-loader",
                    options: {
                        presets: ["@babel/preset-env", "@babel/preset-react"],
                    },
                },
            },
            {
                test: /\.css$/i,
                use: [MiniCssExtractPlugin.loader, "css-loader"],
            },
        ],
    },
    devtool: "source-map", // Add this option for source mapping
};